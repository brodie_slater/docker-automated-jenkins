# Automated Jenkins install on Docker

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a186e74395544bd6b59e081dddc56b20)](https://www.codacy.com/app/brodieslater/docker-automated-jenkins?utm_source=brodie_slater@bitbucket.org&amp;utm_medium=referral&amp;utm_content=brodie_slater/docker-automated-jenkins&amp;utm_campaign=Badge_Grade)
[![Docker Cloud Build Status](https://img.shields.io/docker/cloud/build/brodster22/docker-automated-jenkins.svg)](https://cloud.docker.com/repository/docker/brodster22/docker-automated-jenkins/builds)
[![Docker Cloud Automated build](https://img.shields.io/docker/cloud/automated/brodster22/docker-automated-jenkins.svg)](https://cloud.docker.com/repository/docker/brodster22/docker-automated-jenkins/general)

## Overview

The purpose of this repo is to create a docker container that is fully configured with both settings pre configured and plugins pre installed and no manual interaction necessary to configure the Jenkins instance.

## Credits

Thanks to the following for their awesome guides:

- technologyconversations.com: [Automating Jenkins Docker Setup](https://technologyconversations.com/2017/06/16/automating-jenkins-docker-setup/)
